export function generateReducer(types, initialState = {
  isFetching: false,
  result: null,
  error:false
}){

  return function reducer(state = initialState, action) {
    switch (action.type) {
      case types[0]:
      // request
      return {
        ...state,
        isFetching: true
      };
      case types[1]:
      // success
      return {
        ...state,
        isFetching: false,
        result:action.data,
        error:false
      };
      // failure
      case types[2]:
      return {
        ...state,
        isFetching: false,
        error: action.data || true
      };
      default:
      return state;
    }
  }
}
