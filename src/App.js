import React from 'react';

// redux imports
import { Router, Route, Switch } from 'react-router-dom';
import thunk from 'redux-thunk';
import {createStore, applyMiddleware, compose} from 'redux';
import {Provider} from 'react-redux';
import createHistory from 'history/createBrowserHistory';
import {routerMiddleware} from 'react-router-redux';
import reducers from './ducks/index';

// components
import Header from './components/Header';
import Gallery from './components/Gallery';
import Profile from './components/Profile';

// app config
const history = createHistory();
const middleware = [routerMiddleware(history), thunk];
let devtools;
let composedMiddleware;
// devtools turned off on production:
if(process.env.NODE_ENV === 'production' || !window.__REDUX_DEVTOOLS_EXTENSION__){
    composedMiddleware = applyMiddleware(...middleware);
} else {
    devtools = window.__REDUX_DEVTOOLS_EXTENSION__();
    composedMiddleware = compose(applyMiddleware(...middleware), devtools);
}

export const store = createStore(reducers, composedMiddleware);


const App = () => (
  <Provider store={store}>
    <Router history={history}>
      <div className="container">
        <div className="row">
          <div className="lg-8 lg-offset-2 md-10 md-offset-1">
            <Route component={Header} />
            <div className="content-container">
              <Switch>
                <Route exact path="/" component={Profile} />
                <Route exact path="/gallery" component={Gallery} />
              </Switch>
            </div>
          </div>
        </div>
      </div>
    </Router>
  </Provider>
);

export default App;
