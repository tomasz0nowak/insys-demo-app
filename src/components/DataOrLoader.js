import React from 'react';


  const Loader = () => (
    <div className="loader">
      <img
        src="/images/loader.svg" alt="Trwa wczytywanie"
      />
    </div>
  );

  const DataOrLoader = ({isFetching, children}) => {
    if(isFetching) return <Loader />
    return <div>{children}</div>;
  };

  export default DataOrLoader;
