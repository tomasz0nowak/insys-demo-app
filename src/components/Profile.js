import React from 'react';
import MarkerIcon from 'react-icons/lib/fa/map-marker';


const Profile = () => (
  <div className="profile">
    <h1>Marilyn Monroe</h1>
    <div className="user-location">
      <MarkerIcon className="icon-marker" />
      <span>Poznan, PL</span>
    </div>
    <div className="profile-content">
      <p>
        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec non rhoncus libero.
        Curabitur ultrices tellus in tortor consequat, eget condimentum elit porttitor.
      </p>
      <blockquote>
        Pellentesque luctus pulvinar mauris, in rutrum erat sagittis ac.
        Pellentesque et ipsum nec mauris consequat scelerisque. Integer vel consectetur libero.
        Proin eget porttitor nulla. Pellentesque et ipsum nec mauris consequat scelerisque.
      </blockquote>
      <p>
        Suspendisse suscipit elit at volutpat convallis. Nam blandit faucibus leo vitae tincidunt.
        Quisque laoreet blandit molestie. Pellentesque sit amet erat pulvinar, commodo nibh non, feugiat lectus.
        Integer at mauris velit. Cras nec justo accumsan, tristique magna vitae, eleifend ipsum.
        Suspendisse vestibulum posuere tellus.
      </p>
    </div>
  </div>
);

export default Profile;
