import React from 'react';
import {connect} from 'react-redux';
import {getGallery} from '../ducks/gallery';
import DataOrLoader from '../components/DataOrLoader';

class Gallery extends React.Component {

  componentDidMount() {
    this.props.dispatch(getGallery());
  }

  render() {
    const {gallery, isFetching} = this.props;

    return (
      <div className="gallery">
        <DataOrLoader isFetching={isFetching}>
          {gallery.map(item => (
            <a href={item.link} target="_blank" key={item.published} className="md-4 sm-6 xs-12 photo">
              <img src={item.media.m} alt="" />
            </a>
          ))}
        </DataOrLoader>
      </div>
    )
  }
}

function mapStateToProps(state, props) {
  return {
    gallery: state.gallery.result || [],
    isFetching: state.gallery.isFetching && !state.gallery.result
  }
}

export default connect(mapStateToProps)(Gallery);
