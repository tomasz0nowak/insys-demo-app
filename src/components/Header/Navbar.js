import React from 'react';
import {NavLink} from 'react-router-dom';
import GalleryIcon from 'react-icons/lib/fa/image';
import ProfileIcon from 'react-icons/lib/fa/user';


const Navbar = () => (
  <div className="navbar clearfix">
    <ul>
      <li>
        <NavLink
          exact
          activeClassName="active"
          to="/"
        >
          <ProfileIcon />
        </NavLink>
      </li>
      <li>
        <NavLink
          exact
          activeClassName="active"
          to="/gallery"
        >
          <GalleryIcon />
        </NavLink>
      </li>
    </ul>
  </div>
);

export default Navbar;
