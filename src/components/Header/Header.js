import React from 'react';
import Navbar from './Navbar';


const Header = () => (
  <header>
    <div className="bg">
      <img src="/images/bg.jpg" alt="Background - sand & stones" />
    </div>
    <Avatar />
    <Navbar />
  </header>
);


const Avatar = () => (
  <div className="avatar">
    <img src="/images/avatar.jpg" alt="Marilyn avatar" />
  </div>
);

export default Header;
