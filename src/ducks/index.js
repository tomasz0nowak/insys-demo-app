import {combineReducers} from 'redux';
import { routerReducer } from 'react-router-redux';

import gallery from './gallery';

const root = combineReducers({
    gallery,
    router: routerReducer
});

export default root;
