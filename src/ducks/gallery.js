import {generateReducer} from '../utilities';
import jsonp from 'jsonp';
import {store} from '../App';

const REQUEST = '/gallery/get/REQUEST';
const SUCCESS = '/gallery/get/SUCCESS';
const FAILURE = '/gallery/get/FAILURE';

const reducer = generateReducer([REQUEST, SUCCESS, FAILURE]);

// flickr public feed API accepts only jsonp method, so it needs a global callback function
window.jsonFlickrFeed = function(data) {
  // get only 9 items from all 20; flickr public feed API don't provide query string parameter like per_page
  const slicedData = data.items.slice(0, 9);
  store.dispatch({type:SUCCESS, data: slicedData});
};

export function getGallery() {
  return dispatch => {
    dispatch({type: REQUEST});
    // flickr public feed API accepts only jsonp method.
    jsonp('https://api.flickr.com/services/feeds/photos_public.gne?tags=marilynmonroe&jsoncallback=&format=json', null);
  }
}

export default reducer;
