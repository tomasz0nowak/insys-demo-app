Insys recruitment demo app

Instalacja na Ubuntu:
```
git clone <repozytorium>
cd <nazwaprojektu>
```
```
sudo apt-get install npm
sudo apt-get install nodejs

lub w razie problemów na starszej wersji systemu:

sudo apt-get install nodejs-legacy
```
```
npm install
sudo npm install -g pushstate-server
```
W razie problemów z instalacją node / npm: https://www.digitalocean.com/community/tutorials/how-to-install-node-js-on-ubuntu-16-04

Uruchamianie na buildzie developerskim:
```
npm start
```
Uruchamianie na buildzie produkcyjnym:
```
npm run build
pushstate-server build
```

Zmiana domyślnego portu:
Należy utworzyć nowy skrypt w package.json w postaci:
```
"dowolna-nazwa": "PORT=5000 pushstate-server build",
```
----------------------------------------------------